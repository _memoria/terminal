## Рефакторинг

Посмотрите на код:

```javascript
function func(s, a, b) {

    if (s.match(/^$/)) {
        return -1;
    }

    var i = s.length -1;
    var aIndex =     -1;
    var bIndex =     -1;

    while ((aIndex == -1) && (bIndex == -1) && (i > 0)) {
        if (s.substring(i, i +1) == a) {
            aIndex = i;
        }
        if (s.substring(i, i +1) == b) {
            bIndex = i;
        }
        i = i - 1;
    }

    if (aIndex != -1) {
        if (bIndex == -1) {
            return aIndex;
        }
        else {
            return Math.max(aIndex, bIndex);
        }
    }

    if (bIndex != -1) {
        return bIndex;
    }
    else {
        return -1;
    }
}
```

Что можно улучшить? Как бы вы его переписали?


```javascript

/**
 * @param  {string} s строка
 * @param  {string} a подстрока 1
 * @param  {string} b подстрока 2
 * @return {number}   позиция подстроки
 */
function func2(s, a, b) {
    // проверить, что s имеет строковый тип
    if (typeof s !== "string") return -1;
    // // привести s в строковый тип
    // if (typeof s !== "string") s.toString();

    // проверить, что строка не пустая
    if (!s.length) return -1;

    // поиск подстроки
    var aIndex = s.indexOf(a);
    var bIndex = s.indexOf(b);

    // вернуть большее из двух значений
    return Math.max(aIndex, bIndex);
}

// проверка
console.log(func("", "w", "r"),       func2("", "w", "r"));
console.log(func("qwerty", "w", "r"), func2("qwerty", "w", "r"));
console.log(func("qwerty", "w", "q"), func2("qwerty", "w", "q"));
console.log(func(100, 2, "q"),        func2(100, 2, "q"));
```

## Практическое задание


``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
