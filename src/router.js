import Vue from 'vue'
import VueRouter from 'vue-router'

import Payments  from './components/Payments.vue'
import Operators from './components/Operators.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        { path: '/', component: Operators },
        { path: '/operator/', component: Operators },
        {
            path: '/payment/:id',
            component: Payments,
            props: true
        }
    ]
})

export default router
