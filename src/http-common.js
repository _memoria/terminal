import axios from 'axios'
import MockAdapter from 'axios-mock-adapter';

var mock = new MockAdapter(axios, { delayResponse: 100 });

mock.onGet('/operators').reply(200, [
    { id: 1, title: 'МТС',     color: "#fc1204", logo: "" },
    { id: 2, title: 'Билайн',  color: "#f4ca3e", logo: "" },
    { id: 3, title: 'Мегафон', color: "#24a62c", logo: "" }
  ]
);

mock.onPost('/payment').reply(function() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            if (Math.random() >= 0.5) {
                resolve([200, { success: true } ]);
            } else {
                resolve([200, { success: false } ]);
            }
        }, 700);
    });
});

export const HTTP = axios.create({
    baseURL: `http://localhost:8081/index.php`,
    headers: {
        // Authorization: 'Bearer {token}'
    }
})
