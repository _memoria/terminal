import Vue from 'vue'
import Vuex from 'vuex'
import {HTTP} from './http-common';

Vue.use(Vuex)

var store = new Vuex.Store({
    state: {
        operators: {},
        operatorIndex: 0
    },
    getters: {
        getOperators (state) {
            return state.operators;
        },
        getOperator (state) {
            return state.operators[state.operatorIndex];
        }
    },
    mutations: {
        setOperators (state, payload) {
            state.operators = payload;
        },
        setOperator (state, payload) {
            state.operatorIndex = payload-1 || 0;
        },
    },
    actions: {
        loadOperators () {
            HTTP.get("/operators").then((response) => {
                this.commit('setOperators', response.data);
            });
        }
    }
});

export default store
