import Vue from 'vue'
import App from './components/App.vue'
import store  from './store'
import router from './router'

new Vue({
    el: '#app',
    router: router,
    store:  store,
    render: function render(h) {
        return h(App);
    },
    created: function() {
        this.$store.dispatch('loadOperators');
    },
    watch: {
        '$route.params.id': function (id) {
            this.$store.commit('setOperator', id);
        }
    },
});
